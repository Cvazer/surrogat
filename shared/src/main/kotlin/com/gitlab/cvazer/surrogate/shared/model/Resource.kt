package com.gitlab.cvazer.surrogate.shared.model

data class Resource(
    val name: String, val path: String,
    val clazz: String, val fileName: String,
    val type: Type
) {
    enum class Type {
        ATLAS, TEXTURE
    }

    fun location() = resLocation(this)

    companion object {
        @JvmStatic fun resLocation(res: ResourcePackage) = resLocation(res.path, res.fileName)
        @JvmStatic fun resLocation(res: Resource) = resLocation(res.path, res.fileName)
        @JvmStatic fun resLocation(path: String, filename: String) = "$path/$filename"
    }
}