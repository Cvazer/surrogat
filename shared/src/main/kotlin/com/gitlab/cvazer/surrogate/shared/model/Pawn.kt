package com.gitlab.cvazer.surrogate.shared.model

data class Pawn(
    val id: String,
    var atomLocator: String,
    val props: MutableMap<String, Any>,
    val pos: Pos = Pos(),
)