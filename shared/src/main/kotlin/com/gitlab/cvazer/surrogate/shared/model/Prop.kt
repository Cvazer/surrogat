package com.gitlab.cvazer.surrogate.shared.model

data class Prop(val name: String, val clazz: String)