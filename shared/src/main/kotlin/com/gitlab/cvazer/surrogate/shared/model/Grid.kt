package com.gitlab.cvazer.surrogate.shared.model

import java.util.UUID

data class Grid(
    var id: String = UUID.randomUUID().toString(),
    val atomLocator: String = "grid",
    val type: GridType = GridType.BASE_GRID,
    val pos: Pos = Pos(0, 0),
    val objects: MutableList<GameObject> = mutableListOf(),
    val pawns: MutableList<Pawn> = mutableListOf(),
    val children: MutableList<Grid> = mutableListOf(),
) {
    enum class GridType {
        BASE_GRID
    }

    fun getAtomLocators() = Companion.getAtomLocators(this)

    companion object {
        @JvmStatic fun getAtomLocators(grid: Grid): List<String> {
            val pawns = grid.pawns.map { it.atomLocator }
            return grid.objects
                .map { it.atomLocator }
                .toMutableList()
                .apply { addAll(pawns) }
        }
    }
}