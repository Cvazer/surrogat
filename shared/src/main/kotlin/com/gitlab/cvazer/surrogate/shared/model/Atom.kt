package com.gitlab.cvazer.surrogate.shared.model

data class Atom(
    val locator: String,
    val resources: List<Resource> = listOf(),
    val mandatoryProps: List<String> = listOf(),
    val atomProps: Map<String, Any> = mapOf()
) {
    fun path() = locatorToPath(locator)

    companion object {
        @JvmStatic fun locatorToPath(locator: String) = "data.atoms.${locator}"
            .replace('.', '/', true)+"/atom.json"

    }
}