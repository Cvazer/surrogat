package com.gitlab.cvazer.surrogate.game.atom

interface AtomStrategy {
    fun getType(): String
}