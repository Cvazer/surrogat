package com.gitlab.cvazer.surrogate.ecs.event

import com.badlogic.gdx.math.Vector2

data class EntityVelChangeData(val entityId: String, val vec: Vector2)
