package com.gitlab.cvazer.surrogate.ecs

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Entity

class ListenedEntity(private val engine: ComponentListeningEngine): Entity() {
    override fun add(component: Component?): Entity {
        return super.add(component).also { notifyWasAdded(component) }
    }

    override fun <T : Component?> addAndReturn(component: T): T {
        return super.addAndReturn(component).also { notifyWasAdded(component) }
    }

    override fun <T : Component?> remove(componentClass: Class<T>?): T {
        return super.remove(componentClass).also { notifyWasRemoved(it) }
    }

    override fun removeAll() {
        val components = components.toArray()
        super.removeAll()
        components.forEach { notifyWasAdded(it) }
    }

    fun add(block: ListenedEntity.() -> Component): ListenedEntity {
        add(block.invoke(this))
        return this
    }

    private fun notifyWasAdded(component: Component?){
        component?.also { engine.componentWasAdded(this) }
    }

    private fun notifyWasRemoved(component: Component?){
        component?.also { engine.componentWasRemoved(this) }
    }
}