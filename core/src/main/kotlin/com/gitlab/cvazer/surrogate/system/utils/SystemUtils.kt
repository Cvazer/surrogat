package com.gitlab.cvazer.surrogate.system.utils

import com.gitlab.cvazer.surrogate.getBean
import com.gitlab.cvazer.surrogate.shared.model.Resource
import com.gitlab.cvazer.surrogate.system.service.AssetManagerService

inline fun <reified U> Resource.get(): U = getBean<AssetManagerService>().getAsset(this)