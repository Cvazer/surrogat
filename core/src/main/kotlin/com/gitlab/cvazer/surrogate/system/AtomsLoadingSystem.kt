package com.gitlab.cvazer.surrogate.system

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.async.AsyncExecutor
import com.badlogic.gdx.utils.async.AsyncResult
import com.github.czyzby.autumn.annotation.Component
import com.github.czyzby.autumn.annotation.Inject
import com.gitlab.cvazer.surrogate.Orchestrator
import com.gitlab.cvazer.surrogate.ecs.components.AtomComp
import com.gitlab.cvazer.surrogate.jsonString
import com.gitlab.cvazer.surrogate.readAs
import com.gitlab.cvazer.surrogate.shared.model.Atom
import com.gitlab.cvazer.surrogate.shared.model.ResourcePackage
import com.gitlab.cvazer.surrogate.system.service.AssetManagerService

@Component
class AtomsLoadingSystem {
    @Inject private lateinit var assetManagerService: AssetManagerService
    @Inject private lateinit var orchestrator: Orchestrator

    private val open = mutableSetOf<String>()
    private val atomStates = mutableMapOf<String, AtomState>()
    private var readyAtoms = mutableMapOf<String, Atom>()
    private var sealed = mutableSetOf<String>()

    private var executor = AsyncExecutor(1, "AtomsLoadingSystem")
    private var fetchAtomTasks = mutableListOf<Pair<String, AsyncResult<Atom>>>()
    private var fetchResourceTasks = mutableListOf<AsyncResult<ResourcePackage>>()

    private fun fetchAtoms(){
        fetchAtomTasks
            .filter { it.second.isDone }
            .onEach {
                Gdx.files.local(Atom.locatorToPath(it.first))
                    .writeString(it.second.get().jsonString(), false)
            }
        fetchAtomTasks.removeIf { it.second.isDone && Gdx.files.local(Atom.locatorToPath(it.first)).exists() }
    }

    private fun fetchResources(){
        val tmpResSet = mutableSetOf<String>()
        fetchResourceTasks
            .filter { it.isDone }.map { it.get() }
            .onEach { assetManagerService.saveResourcePackage(it) }
            .onEach { tmpResSet.add(it.location()) }
        fetchResourceTasks
            .removeIf { it.isDone && tmpResSet.contains(it.get().location()) }
    }

    private fun processAtom(locator: String, state: AtomState) {
        //Start fetching missing atom files
        if (!state.atomFetched && !state.isAtomFetching) {
            fetchAtomTasks.add(Pair(locator, executor.submit { orchestrator.backendApi.loadAtom(locator) }))
            state.isAtomFetching = true
        }

        //Load missing atoms to memory
        if (state.atomFetched && !state.atomLoaded) {
            readyAtoms[locator] = Gdx.files.local(Atom.locatorToPath(locator)).readString().readAs()
        }

        //Start fetching missing resource files
        if (!state.resFetched && !state.isResFetching && state.atomLoaded) {
            readyAtoms[locator]!!.resources
                .filter { assetManagerService.isResourceAvailable(it) }
                .map { executor.submit { orchestrator.backendApi.loadResource(it) } }
                .forEach { fetchResourceTasks.add(it) }
            state.isResFetching = true
        }

        //Start loading all missing assets
        if (!state.assetLoaded && !state.isAssetLoading && state.atomLoaded && state.resFetched) {
            readyAtoms[locator]!!.resources
                .filter { !assetManagerService.assets.containsKey(it.location()) }
                .forEach { assetManagerService.loadAsset(it) }
            state.isAssetLoading = true
        }

        fetchAtoms()
        fetchResources()
    }

    fun update(){
        open.map { Pair(it, atomStates[it]!!) }
            .onEach { updateAtomState(it.first, it.second) }
            .onEach { if (it.second.ready) sealed.add(it.first) }
            .filter { !it.second.ready }
            .forEach { processAtom(it.first, it.second) }
        open.removeIf { atomStates[it]!!.ready && sealed.contains(it) }
    }

    private fun updateAtomState(locator: String, state: AtomState) {
        //Atom already processed
        if (state.ready) return

        //Check if atom is already fetched
        state.atomFetched = Gdx.files.local(Atom.locatorToPath(locator)).exists()

        //Reset fetching flag if atom fetching is done
        if (state.isAtomFetching && state.atomFetched) state.isAtomFetching = false

        //Check if atom is loaded
        state.atomLoaded = readyAtoms.containsKey(locator)

        //Check if all resources exist in file system. Check only if atom is already loaded.
        //If it is not - there is nowhere to get his resource info from!
        state.resFetched = state.atomLoaded && readyAtoms[locator]!!.resources
            .all { assetManagerService.isResourceAvailable(it) }

        //Reset fetching flag if all missing resources fetching is done
        if (state.isResFetching && state.resFetched) state.isResFetching = false

        //Check if all assets from resources are loaded. Check only if they are fetched
        //If not - there is nothing to load!
        state.assetLoaded = state.resFetched && state.atomLoaded && readyAtoms[locator]!!.resources
            .all { assetManagerService.isAssetForResourceLoaded(it) }

        //Reset loading flag if all respective to resources assets are loaded
        if (state.isAssetLoading && state.assetLoaded) state.isAssetLoading = false

        //Mark as ready if atom, resources and assets are done
        state.ready = state.atomFetched && state.atomLoaded && state.resFetched && state.assetLoaded
    }

    fun getAtom(atom: AtomComp) = getAtom(atom.locator)
    fun getAtom(locator: String): Atom? {
        if (sealed.contains(locator)) return readyAtoms[locator]!!
        if (open.contains(locator)) return null
        loadAtom(locator)
        return null
    }

    fun isAtomReady(atom: AtomComp): Boolean = isAtomReady(atom.locator)
    fun isAtomReady(locator: String): Boolean = sealed.contains(locator)

    private fun loadAtom(locator: String) = loadAtom(setOf(locator))
    fun loadAtom(locators: Collection<String>) = locators.forEach {
        if (open.contains(it)) return@forEach
        if (sealed.contains(it)) return@forEach
        atomStates.putIfAbsent(it, AtomState(it))
        open.add(it)
    }

    fun allReady() = open.isEmpty()

    data class AtomState(
        val locator: String,
        var atomFetched: Boolean = false, var isAtomFetching: Boolean = false, var atomLoaded: Boolean = false,
        var resFetched: Boolean = false, var isResFetching: Boolean = false,
        var assetLoaded: Boolean = false, var isAssetLoading: Boolean = false,
        var ready: Boolean = false
    )
}