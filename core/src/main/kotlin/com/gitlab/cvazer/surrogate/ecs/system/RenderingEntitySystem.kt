package com.gitlab.cvazer.surrogate.ecs.system

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Vector3
import com.github.czyzby.autumn.annotation.Component
import com.github.czyzby.autumn.annotation.Inject
import com.gitlab.cvazer.surrogate.ecs.LightEntitySystem
import com.gitlab.cvazer.surrogate.ecs.components.*
import com.gitlab.cvazer.surrogate.ecs.resource
import com.gitlab.cvazer.surrogate.game.GameUtils
import com.gitlab.cvazer.surrogate.game.atom.AtomStrategyRegistry
import com.gitlab.cvazer.surrogate.game.atom.types.RenderingStrategy
import com.gitlab.cvazer.surrogate.system.AtomsLoadingSystem
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.ashley.mapperFor
import ktx.graphics.use

@Component
open class RenderingEntitySystem: LightEntitySystem() {
    @Inject private lateinit var atomSystemRegistry: AtomStrategyRegistry
    @Inject private lateinit var atomLoadingSystem: AtomsLoadingSystem

    private val transformMapper = mapperFor<Transform>()
    private val atomMapper = mapperFor<AtomComp>()
    private val propsMapper = mapperFor<Props>()
    private val renderingMapper = mapperFor<WorldRendering>()

    private lateinit var spriteBatch: SpriteBatch
    private lateinit var shapeRenderer: ShapeRenderer
    private lateinit var cursorTexture: Texture
    private lateinit var camera: OrthographicCamera

    override fun init(){
        spriteBatch = SpriteBatch()
        shapeRenderer = ShapeRenderer().apply { this.setAutoShapeType(true) }
        camera = OrthographicCamera(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
        cursorTexture = resource("cursorTexture")
    }

    override fun update(deltaTime: Float) {
        camera.update()
        entities
            .sortedBy { (it[renderingMapper] ?: return@sortedBy 0).zLevel }
            .forEach { updateEntity(it, deltaTime) }
        val cursorPos = camera.unproject(Vector3(
            resource("cursorX"),
            resource("cursorY"),
            0f
        ))
        spriteBatch.use {
            it.draw(cursorTexture, cursorPos.x, cursorPos.y - cursorTexture.height)
        }
        drawDebugNet()
    }

    private fun drawDebugNet() {
        shapeRenderer.projectionMatrix = camera.combined
        shapeRenderer.begin()
        for (i in -10..10){
            val y = i * GameUtils.PIXELS_PER_UNIT - (GameUtils.PIXELS_PER_UNIT / 2)
            shapeRenderer.line(-2000f, y, 2000f, y)
            for (k in -10..10){
                val x = k * GameUtils.PIXELS_PER_UNIT - (GameUtils.PIXELS_PER_UNIT / 2)
                shapeRenderer.line(x, -2000f, x, 2000f)
            }
        }
        shapeRenderer.end()
    }

    override fun updateEntity(entity: Entity, delta: Float) {
        val transform = entity[transformMapper]!!
        val atom = entity[atomMapper]!!
        val props = entity[propsMapper]
        val id = entity[idMapper]

        if (!atomLoadingSystem.isAtomReady(atom)) return

        onPlayerEntity(id) { camera.position.set(transform.x, transform.y, 0f) }
//        camera.position.set(transform.x, transform.y, 0f)

        spriteBatch.projectionMatrix = camera.combined
        spriteBatch.begin()
        atomSystemRegistry.getBestMatchingStrategy(atom, RenderingStrategy::class)
            .render(spriteBatch, atomLoadingSystem.getAtom(atom)!!, transform, props, entity)
        spriteBatch.end()
    }

    override fun getFamilyBuilder(): Family.Builder = allOf(
        Transform::class, AtomComp::class, WorldRendering::class
    ).exclude(GridWorld::class.java)

    override fun dispose() {
        spriteBatch.dispose()
        shapeRenderer.dispose()
    }
}