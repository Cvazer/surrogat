package com.gitlab.cvazer.surrogate.game

abstract class GameStageAdapter: GameStage {
    override fun init() {}
    override fun tick(delta: Float) {}
    override fun dispose() {}
    override fun resize(width: Int, height: Int) {}
}