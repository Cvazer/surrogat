package com.gitlab.cvazer.surrogate.game

import com.badlogic.gdx.Input
import com.badlogic.gdx.InputAdapter
import com.github.czyzby.autumn.annotation.Component
import com.github.czyzby.autumn.annotation.Inject
import com.gitlab.cvazer.surrogate.ecs.EcsSystemResourceRegistry
import com.gitlab.cvazer.surrogate.ecs.LightEntitySystem
import com.gitlab.cvazer.surrogate.ecs.event.Command
import com.gitlab.cvazer.surrogate.ecs.event.EntityVelChangeData
import ktx.math.vec2

@Component
class GameInputProcessor: InputAdapter() {
    @Inject private lateinit var registry: EcsSystemResourceRegistry

    override fun keyDown(keycode: Int): Boolean {
        when(keycode) {
            Input.Keys.W -> LightEntitySystem.eventList.add(Command(
                Command.Type.BODY_VELOCITY_CHANGE.value,
                EntityVelChangeData(
                    vec = vec2(0f, 2f),
                    entityId = registry.getResource("charId")
                )
            ))
            Input.Keys.S -> LightEntitySystem.eventList.add(Command(
                Command.Type.BODY_VELOCITY_CHANGE.value,
                EntityVelChangeData(
                    vec = vec2(0f, -2f),
                    entityId = registry.getResource("charId")
                )
            ))
        }
        return true
    }

    override fun keyUp(keycode: Int): Boolean {
        when(keycode) {
            Input.Keys.W -> LightEntitySystem.eventList.add(Command(
                Command.Type.BODY_VELOCITY_CHANGE.value,
                EntityVelChangeData(
                    vec = vec2(0f, -2f),
                    entityId = registry.getResource("charId")
                )
            ))
            Input.Keys.S -> LightEntitySystem.eventList.add(Command(
                Command.Type.BODY_VELOCITY_CHANGE.value,
                EntityVelChangeData(
                    vec = vec2(0f, +2f),
                    entityId = registry.getResource("charId")
                )
            ))
        }
        return true
    }

    override fun keyTyped(character: Char): Boolean {
        return true
    }

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        registry.addResource(screenX.toFloat(), Float::class.java, "cursorX")
        registry.addResource(screenY.toFloat(), Float::class.java, "cursorY")
        return true
    }
}