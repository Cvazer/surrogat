package com.gitlab.cvazer.surrogate.game.atom.types

import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.World
import com.gitlab.cvazer.surrogate.ecs.components.AtomComp
import com.gitlab.cvazer.surrogate.ecs.components.Position
import com.gitlab.cvazer.surrogate.ecs.components.Props
import com.gitlab.cvazer.surrogate.game.StrategyCategories.PHYSICS_STRAT
import com.gitlab.cvazer.surrogate.game.atom.AtomStrategy

interface PhysicsStrategy: AtomStrategy {
    fun create(world: World, positionComp: Position, atomComp: AtomComp, propsComp: Props?): Body
    override fun getType(): String = PHYSICS_STRAT
}