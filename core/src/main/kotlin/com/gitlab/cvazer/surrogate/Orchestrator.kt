package com.gitlab.cvazer.surrogate

import com.github.czyzby.autumn.annotation.Component
import com.github.czyzby.autumn.annotation.Destroy
import com.github.czyzby.autumn.annotation.Inject
import com.gitlab.cvazer.surrogate.backend.BackendApi
import com.gitlab.cvazer.surrogate.backend.BackendApiAdapter
import com.gitlab.cvazer.surrogate.game.GameStage
import com.gitlab.cvazer.surrogate.game.GameStageAdapter
import com.gitlab.cvazer.surrogate.system.AtomsLoadingSystem
import com.gitlab.cvazer.surrogate.system.service.AssetManagerService

@Component
class Orchestrator {
    @Inject private lateinit var atomsLoadingSystem: AtomsLoadingSystem
    @Inject private lateinit var assetManagerService: AssetManagerService

    private var currentStage: GameStage = object: GameStageAdapter(){}
    var backendApi: BackendApi = object: BackendApiAdapter(){}

    fun tick(delta: Float) {
        assetManagerService.assetManager.update()
        atomsLoadingSystem.update()
        currentStage.tick(delta)
    }

    fun resize(width: Int, height: Int) {
        currentStage.resize(width, height)
    }

    @Destroy fun dispose() {
        currentStage.dispose()
    }

    fun setGameState(clazz: Class<*>) {
        val candidate = Application.context.getComponent(clazz)
        if (candidate is GameStage) {
            currentStage.dispose()
            candidate.init()
            currentStage = candidate
        } else {
            log().error { "${candidate::class.simpleName} is not instance of ${GameStage::class.simpleName}" }
            return
        }

    }
}