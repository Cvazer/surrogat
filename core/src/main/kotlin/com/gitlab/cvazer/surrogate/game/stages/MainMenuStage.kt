package com.gitlab.cvazer.surrogate.game.stages

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.Stage
import com.github.czyzby.autumn.annotation.Component
import com.github.czyzby.autumn.annotation.Inject
import com.gitlab.cvazer.surrogate.Orchestrator
import com.gitlab.cvazer.surrogate.game.GameStage
import ktx.actors.onClick
import ktx.actors.stage
import ktx.scene2d.actors
import ktx.scene2d.horizontalGroup
import ktx.scene2d.verticalGroup
import ktx.scene2d.vis.visTextButton

@Component
class MainMenuStage : GameStage {
    @Inject private lateinit var orchestrator: Orchestrator

    private lateinit var stage: Stage

    override fun init() {
        stage = stage()
        Gdx.input.inputProcessor = stage

        stage.actors {
            verticalGroup {
                setFillParent(true)
                center()
                space(20f)
                horizontalGroup {
                    visTextButton(text = "Single Player", style = "main-menu") {
                        onClick { orchestrator.setGameState(PlayGameStage::class.java) }
                    }
                }
                horizontalGroup {
                    visTextButton(text = "Multi Player", style = "main-menu")
                }
                horizontalGroup {
                    visTextButton(text = "Exit", style = "main-menu")
                }
            }
        }

        stage.isDebugAll = true
    }

    override fun tick(delta: Float) {
        stage.act(delta)
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height, true)
    }

    override fun dispose() {
        stage.dispose()
    }
}