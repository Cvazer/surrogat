package com.gitlab.cvazer.surrogate.ecs.event

data class Command(val command: String, val value: Any) {
    enum class Type(val value: String) {
        BODY_VELOCITY_CHANGE("BODY_VELOCITY_CHANGE")
    }
}