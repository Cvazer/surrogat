package com.gitlab.cvazer.surrogate.ecs.system

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.physics.box2d.Box2D
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.github.czyzby.autumn.annotation.Component
import com.gitlab.cvazer.surrogate.ecs.LightEntitySystem
import com.gitlab.cvazer.surrogate.ecs.components.GridWorld
import com.gitlab.cvazer.surrogate.ecs.components.Position
import com.gitlab.cvazer.surrogate.ecs.resource
import com.gitlab.cvazer.surrogate.game.GameUtils
import ktx.ashley.allOf
import ktx.ashley.get
import ktx.ashley.mapperFor
import ktx.box2d.createWorld

@Component
class GridPhysicsEntitySystem: LightEntitySystem() {
    private val gridWorldMapper = mapperFor<GridWorld>()
//    private val positionMapper = mapperFor<Position>()

    private lateinit var debugRenderer: Box2DDebugRenderer
    private lateinit var debugCamera: OrthographicCamera

    override fun init() {
        Box2D.init()
        debugRenderer = Box2DDebugRenderer()
        debugCamera = OrthographicCamera(
            Gdx.graphics.width / GameUtils.PIXELS_PER_UNIT,
            Gdx.graphics.height / GameUtils.PIXELS_PER_UNIT
        ).apply { resource(this, "box2dDebugCamera") }
    }

    override fun removedFromEngine(engine: Engine?) {
        engine?.getEntitiesFor(getFamilyBuilder().get())?.forEach { dispose(it) }
        super.removedFromEngine(engine)
    }

    override fun updateEntity(entity: Entity, delta: Float) {
        val component = entity[gridWorldMapper]!!
//        val position = entity[positionMapper]!!

        if (component.world == null) {
            component.world = createWorld()
        } else {
            debugCamera.update()
            component.world!!.step(delta.coerceAtMost(0.25f), 6, 2)
//            debugRenderer.render(component.world, debugCamera.combined)
        }
    }

    override fun dispose(entity: Entity?) {
        entity?.let { it[gridWorldMapper]!!.world?.dispose() }
        debugRenderer.dispose()
    }

    override fun getFamilyBuilder(): Family.Builder = allOf(
        GridWorld::class, Position::class
    )
}