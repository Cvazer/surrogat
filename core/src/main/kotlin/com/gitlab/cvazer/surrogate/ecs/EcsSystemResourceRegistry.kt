package com.gitlab.cvazer.surrogate.ecs

import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.GdxRuntimeException
import com.badlogic.gdx.utils.ObjectMap
import com.github.czyzby.autumn.annotation.Component
import com.github.czyzby.autumn.annotation.Initiate
import com.gitlab.cvazer.surrogate.log

@Component
class EcsSystemResourceRegistry {
    private val resources = ObjectMap<Class<*>, Array<Any>>()
    private val resourcesByAlias = ObjectMap<String, Any>()

    @Initiate fun init(){
        resourcesByAlias.put("cursorX", 0f)
        resourcesByAlias.put("cursorY", 0f)
    }

    fun <T: Any> addResource(res: T, clazz: Class<T>) = addResource(res, clazz, null)
    fun <T: Any> addResource(res: T, clazz: Class<T>, alias: String?): T {
        if (!resources.containsKey(clazz)) resources.put(clazz, Array(false, 1))
        resources[clazz]!!.add(res)
        alias?.let { resourcesByAlias.put(it, res) }
        return res
    }

    fun <T: Any> addResource(res: T, alias: String?): T {
        alias?.let { resourcesByAlias.put(it, res) }
        return res
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> getResource(clazz: Class<T>): T {
        val res = resources[clazz]
            ?: throw GdxRuntimeException("No resource for class ${clazz.name}")
        if (res.size == 1) return res[0] as T
        log("CAUTION! There are multiple available resources for class ${clazz.name}")
        return res[0] as T
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> getResource(alias: String): T {
        return resourcesByAlias[alias] as T
            ?: throw GdxRuntimeException("No resource with alias $alias registered")
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> getResourceOrNull(alias: String): T? = resourcesByAlias[alias] as T

    operator fun <T> get(alis: String): T = getResource(alis)

    operator fun set(alias: String, value: Any) = addResource(value, alias)

    //TODO Unit tests (blocked)
}