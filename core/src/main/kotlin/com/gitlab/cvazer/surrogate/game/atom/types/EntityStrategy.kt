package com.gitlab.cvazer.surrogate.game.atom.types

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.gitlab.cvazer.surrogate.game.StrategyCategories.ENTITY_STRAT
import com.gitlab.cvazer.surrogate.game.atom.AtomStrategy

interface EntityStrategy<T>: AtomStrategy {
    fun toEntity(target: T, parent: Entity?, engine: Engine?): List<Entity>
    fun dispose(target: Entity) {}
    override fun getType(): String = ENTITY_STRAT
}