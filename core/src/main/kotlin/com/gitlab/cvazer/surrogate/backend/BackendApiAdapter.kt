package com.gitlab.cvazer.surrogate.backend

import com.gitlab.cvazer.surrogate.shared.model.Atom
import com.gitlab.cvazer.surrogate.shared.model.Grid
import com.gitlab.cvazer.surrogate.shared.model.Resource
import com.gitlab.cvazer.surrogate.shared.model.ResourcePackage

abstract class BackendApiAdapter: BackendApi {
    override fun loadAccountData(key: String) {}
    override fun loadCharData(id: String) {}
    override fun loadGrid(gridId: String): Grid = Grid()
    override fun loadAtom(locator: String): Atom = Atom("")
    override fun loadResource(res: Resource): ResourcePackage = ResourcePackage("", "", "")
    override fun loadCurrentChar(): String? = null
}